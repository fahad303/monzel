@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div>
                    <a style="margin: 19px;" href="{{ url('contacts') }}" class="btn btn-dark">Back to lists</a>
                </div>

                <h1 class="display-3">Invoices</h1>

                <div class="col-sm-12">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>


                <table id="list_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>User Name</td>
                        <td>Invoice Id</td>
                        <td>Payment Amount</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->id}}</td>
                            <td>{{$invoice->user->name}}</td>
                            <td>{{$invoice->invoice_id}}</td>
                            <td>{{$invoice->payment}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('footerJS')

    <script>
    </script>

@endsection

