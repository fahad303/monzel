@extends('layouts.app')

@section('content')

    <div class="container">
        @if (\Session::has('code'))
            <div class="alert alert-{{\Session::get('code')}}">
                {!! \Session::get('message') !!}
            </div>
        @endif

        <h2>Current Investments</h2>
        @if( count($progress_values) )
            @foreach($progress_values as $progress)
                <div class="content">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar"aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$progress->progress}}%">
                            {{$progress->progress}}%
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="content">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar"aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                        0%
                    </div>
                </div>
            </div>

        @endif
            <br/>
            <a href="{{ url('contribute') }}" class="btn btn-primary btn-block">Contribute</a>
    </div>

    @endsection
