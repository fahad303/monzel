@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="display-3">User</h1>

                <div class="col-sm-12">
                    @if(session()->get('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                </div>

                <div>
                    <a style="margin: 19px;" href="{{ route('contacts.create')}}" class="btn btn-primary">New User</a>
                    <a style="margin: 19px;" href="{{ route('progresses.create')}}" class="btn btn-info">add progress</a>
                    <a style="margin: 19px;" href="{{ route('invoices.index')}}" class="btn btn-success">Invoices</a>
                </div>


                <table id="list_table" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Job Title</td>
                        <td>City</td>
                        <td>Country</td>
                        <td colspan=2>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td>{{$contact->id}}</td>
                            <td>{{$contact->first_name}} {{$contact->last_name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->job_title}}</td>
                            <td>{{$contact->city}}</td>
                            <td>{{$contact->country}}</td>
                            <td>
                                <a href="{{ route('contacts.edit',$contact->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('contacts.destroy', $contact->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('footerJS')

    <script>
    </script>

@endsection
