<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contribute extends Model
{
    protected $fillable = [
        'token',
        'correlationid',
        'build'
    ];
}
