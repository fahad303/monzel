<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Progress;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $progresses = Progress::all();
//        return view('welcome', compact('progresses'));

        $contacts = Contact::all();

        return view('contacts.index', compact('contacts'));

    }

    public function about()
    {
        return view('about');
    }
}
