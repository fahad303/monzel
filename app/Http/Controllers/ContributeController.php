<?php
    
    namespace App\Http\Controllers;
    
    use App\Invoice;
    use Illuminate\Http\Request;
    use App\Contribute;
    
    
    use Srmklive\PayPal\Services\ExpressCheckout;
    
    class ContributeController extends Controller
    {
        public $data;
        public function __construct()
        {
            $this->middleware('auth');
            $this->data['items'] = [
                [
                    'name' => 'Product 1',
                    'price' => 9.99,
                    'desc' => 'Description for product 1',
                    'qty' => 1
                ]
            ];
            
            $invoice_id = Invoice::count() + 1;
            $this->data['invoice_id'] = 'INV-00000000_' . $invoice_id;
            $this->data['invoice_description'] = "Order #{$this->data['invoice_id']} Invoice";
            $this->data['return_url'] = url('/contribute/success');
            $this->data['cancel_url'] = url('/contribute/cancel');
            $total = 0;
            foreach ($this->data['items'] as $item) {
                $total += $item['price'] * $item['qty'];
            }
            
            $this->data['total'] = $total;
            
            //give a discount of 10% of the order amount
            $this->data['shipping_discount'] = round((10 / 100) * $total, 2);
        }
        
        public function index()
        {
            $provider = new ExpressCheckout;      // To use express checkout.
            $options = [
                'BRANDNAME' => 'Monzel Investments',
                'LOGOIMG' => public_path('logo/monzel.jpeg'),
                'CHANNELTYPE' => 'Merchant'
            ];
            
            //dd($this->data);
            $response = $provider->addOptions($options)->setExpressCheckout($this->data);
            //dd($response);
            // This will redirect user to PayPal
            return redirect($response['paypal_link']);
        }
        
        public function success(Request $request){
            $token = $request['token'];
            $payerId = $request['PayerID'];
            $provider = new ExpressCheckout();
            $response = $provider->getExpressCheckoutDetails($token);
            
            //dump($request->all(), $response);
            // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
            // we return back with error
            if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
            } else {
                
                // Note that 'token', 'PayerID' are values returned by PayPal when it redirects to success page after successful verification of user's PayPal info.
                $expressCheckoutResponse = $provider->doExpressCheckoutPayment($this->data, $token, $payerId);
                //dd($expressCheckoutResponse);
                
                //creating invoice
                $invoice = new Invoice();
                $invoice->user_id = \Auth::id();
                $invoice->invoice_id = $this->data['invoice_id'];
                $invoice->payment = $expressCheckoutResponse['PAYMENTINFO_0_AMT'];
                $invoice->payment_status = $expressCheckoutResponse['ACK'];
                $invoice->billing_agid = $expressCheckoutResponse['BILLINGAGREEMENTID'];
                $invoice->correlation_id = $expressCheckoutResponse['CORRELATIONID'];
                $invoice->save();
                
                return redirect('/')->with(['code' => 'success', 'message' => 'Payment processed successfully!']);
            }
            
        }
        
    }
